//
//  Common.h
//  Common
//
//  Created by Victor Tatarasanu on 23/03/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Common.
FOUNDATION_EXPORT double CommonVersionNumber;

//! Project version string for Common.
FOUNDATION_EXPORT const unsigned char CommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Common/PublicHeader.h>


