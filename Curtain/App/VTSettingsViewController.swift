//
//  VTSettingsViewController.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 25/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

class VTSettingsViewController: UITableViewController {
    
    let cellIdentifier = "cellIdentifier"
    var tableData = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "VTSettingsCell", bundle: nil), forCellReuseIdentifier: self.cellIdentifier)
        // Setup table data
        for index in 0...5 {
            self.tableData.append("Item \(index)")
        }
    }
    
    // MARK: UITableViewDataSource methods
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? VTSettingsCell else {
            return UITableViewCell()
        }
        cell.title?.text = self.tableData[indexPath.row]
        return cell
    }
    
    // MARK: UITableViewDelegate methods
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
}