//
//  Introduction.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 20/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import Cartography

class VTIntroductionViewController: UIViewController, VTIntroViewDelegate, VTIntroViewDatasource {
    
    var introView: VTIntroView?
    var introScreens: Array<VTIntroViewCell>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIntroView()
        setupIntroPages()
    }
    
    // MARK: Private
    
    func setupIntroView() {
        let intro = VTIntroView()
        intro.delegate = self
        intro.dataSource = self
        view.addSubview(intro)
        
        constrain(self.view, intro) { superview, intro in
            intro.top == superview.top
            intro.bottom == superview.bottom
            intro.right == superview.right
            intro.left == superview.left
        }
    }
    
    func setupIntroPages() {
        let page1 = VTIntroViewCell.loadFromNib()
        page1.setup(UIImage(named: "protect"), title: "Protect", details: "Internet is amazing. We can explore the entire world with a click. But we love our children and we don’t want them exposed to the dark places of the internet.")
        let page2 = VTIntroViewCell.loadFromNib()
        page2.setup(UIImage(named: "simple"), title: "Simple", details: "It’s insanely simple. Just pick inappropriate websites, choose a pin code and you don’t need to worry about your children exposure to damaging information.")
        let page3 = VTIntroViewCellWithActions.loadFromNib()
        page3.setup(UIImage(named: "checkmark"), title: "Done", details: "You are done. You can start protecting your child right away.")
        self.introScreens = [page1, page2, page3]
    }
    
    // MARK: VTIntroViewDelegate
    
    func didTapSkip(sender: VTIntroView) {
        
    }
    
    // MARK: VTIntroViewDatasource
    
    func numberOfPages(sender: VTIntroView) -> Int {
        if let introS = self.introScreens {
            return introS.count
        }
        return 0
    }
    
    func pageAtIndex(index: Int, sender: VTIntroView) -> VTIntroViewCell {
        if let introS = self.introScreens {
            return introS[index]
        }
        return VTIntroViewCell()
    }
    
}