//
//  VTDashboardViewController.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 20/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import Cartography

class VTDashboardViewController: UIViewController, VTBubblesViewDelegate, VTBubblesViewDataSource {
    
    var categories = Array<VTCategory>()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCategories()
        setupBubbles()
        self.view.backgroundColor = UIColor.whiteColor()
    }
    
    // MARK: Private
    
    func setupCategories() {
        let values = ["abortion", "ads", "adult", "aggressive", "antispyware", "artnudes", "astrology", "audio-video", "banking", "beerliquorsale", "blog", "cellphones", "chat", "childcare", "cleaning", "clothing", "contraception", "culnary", "dating", "desktopsillies", "desktop", "dialers", "drugs", "ecommerce", "entertainment", "filehosting", "gambling", "guns", "hacking", "kidstimewasting", "phishing", "religion", "sect", "sexuality", "spyware", "porn", "violence", "warez", "weapons", "viruses"]
        
        values.forEach { (title) in
            let category = VTCategory(build: { (build) in
                build.title = title;
                build.numberOfSites = Int(arc4random_uniform(15000 - 2000) + 2000)
                build.isHighlighted = arc4random() % 2 == 0 ? true : false
            })
            categories.append(category)
        }
    }
    
    func setupBubbles() {
        let intro = VTBubblesView()
        intro.delegate = self
        intro.dataSource = self
        view.addSubview(intro)
        
        constrain(self.view, intro) { superview, intro in
            intro.top == superview.top
            intro.bottom == superview.bottom
            intro.right == superview.right
            intro.left == superview.left
        }
    }
    
    // MARK: VTBubblesViewDataSource
    
    func numberOfBubbles() -> Int {
        return self.categories.count
    }
    
    func bubbleTitle(atIndex:Int) -> String {
        return self.categories[atIndex].title
    }
    
    func isBubbleHighlighted(atIndex:Int) -> Bool {
        return self.categories[atIndex].isHighlighted
    }
    
    func bubbleSubtitle(atIndex:Int) -> String {
        return self.categories[atIndex].details
    }
    
    // MARK: VTBubblesViewDelegate
    
    func didTap(atIndex atIndex: Int) {
        print("tapped")
    }
    
    func bubbleColor() -> UIColor {
        return UIColor.textColor()
    }
    
    func highlightedBubbleColor() -> UIColor {
        return UIColor.currentPageIndicatorColor()
    }
    
}
