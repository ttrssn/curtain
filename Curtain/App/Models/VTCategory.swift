//
//  VTCategory.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 14/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

class VTCategory : NSObject {
    var title: String = ""
    var details: String = ""
    var numberOfSites: Int = 0
    var isHighlighted: Bool = false
    
    required init(build: (VTCategory) -> Void) {
        super.init()
        build(self)
    }
}
