//
//  UIColor+App.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 04/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(hexString: String) {
        let hex = hexString.stringByTrimmingCharactersInSet(NSCharacterSet.alphanumericCharacterSet().invertedSet)
        var int = UInt32()
        NSScanner(string: hex).scanHexInt(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (1, 1, 1, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    class func backgroundColor() -> UIColor {
        return UIColor(hexString: "F4F1F0")
    }
    
    class func textColor() -> UIColor {
        return UIColor(hexString: "675F58")
    }
    
    class func pageIndicatorColor() -> UIColor {
        return UIColor(hexString: "675F58")
    }
    
    class func currentPageIndicatorColor() -> UIColor {
        return UIColor(hexString: "FF931E")
    }
    
    class func random() -> UIColor{
        return UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1.0)
    }
    
    //MARK: Private
    
}