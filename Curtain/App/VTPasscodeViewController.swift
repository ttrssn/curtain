//
//  ViewController.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 22/03/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import Cartography

class VTPasscodeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPasscodeView()
    }
    
    func setupPasscodeView() {
        
            let passcode = VTPassCodeView.loadFromNib()
            view.addSubview(passcode)
            
            constrain(self.view, passcode) { superview, intro in
                intro.top == superview.top
                intro.bottom == superview.bottom
                intro.right == superview.right
                intro.left == superview.left
            }
    }
}

