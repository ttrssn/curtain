//
//  UIFont+App.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 04/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func bodyFontWithSize(size: CGFloat) -> UIFont {
        var font: UIFont
        if NSProcessInfo.processInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 2, patchVersion: 0)) {
            font = UIFont.systemFontOfSize(size, weight: UIFontWeightLight)
        } else {
            if let f = UIFont(name: "HelveticaNeue-Light", size: size) {
                font = f
            } else {
                font = UIFont.systemFontOfSize(size)
            }
        }
        return font
    }
    
    class func headlineFontWithSize(size: CGFloat) -> UIFont {
        var font: UIFont
        if NSProcessInfo.processInfo().isOperatingSystemAtLeastVersion(NSOperatingSystemVersion(majorVersion: 8, minorVersion: 2, patchVersion: 0)) {
            font = UIFont.systemFontOfSize(size, weight: UIFontWeightThin)
        } else {
            if let f = UIFont(name: "HelveticaNeue-Thin", size: size) {
                font = f
            } else {
                font = UIFont.systemFontOfSize(size)
            }
        }
        return font
    }
}
