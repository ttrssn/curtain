//
//  BubblesView.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 07/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import Cartography

@objc protocol VTBubblesViewDelegate {
    optional func didTap(atIndex atIndex: Int);
    optional func bubbleColor() -> UIColor
    optional func highlightedBubbleColor() -> UIColor
}

@objc protocol VTBubblesViewDataSource {
    func numberOfBubbles() -> Int
    func bubbleTitle(atIndex:Int) -> String
    func bubbleSubtitle(atIndex:Int) -> String
    func isBubbleHighlighted(atIndex:Int) -> Bool
}

class VTBubblesView: UIView {

    var bubbleSize = CGSize(width: 80, height: 80)
    var highlightedBubbleSize = CGSize(width: 100, height: 100)
    var bubbleColor = UIColor.redColor()
    var highlightedBubbleColor = UIColor.redColor()
    
    var delegate: VTBubblesViewDelegate?
    var dataSource: VTBubblesViewDataSource?
    
    private var animator: UIDynamicAnimator?
    private var collision: UICollisionBehavior?
    private var viewIsLoaded = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if viewIsLoaded == false {
            self.loadView()
            self.viewIsLoaded = true
        }
    }
    
    // MARK: Private
    
    func loadView() {
        
        if numberOfBubbles() <= 0 {
            return
        }
        
        var views = Array<UIView>()
        
        let animator = UIDynamicAnimator(referenceView: self)
        self.animator = animator
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(VTBubblesView.handlePan(_:)))
        self.addGestureRecognizer(panGesture)
        
        for index in 0...numberOfBubbles() - 1 {
            let title = bubbleTitle(index)
            let subtitle = bubbleSubtitle(index)
            let bubble = VTBubbleView(title: title, subtitle: subtitle)
            self.configureBubble(bubble, atIndex: index)
            let tap = UITapGestureRecognizer(target: self, action: #selector(VTBubblesView.handleTap(_:)))
            bubble.addGestureRecognizer(tap)
            views.append(bubble)
            self.addSubview(bubble)
            self.addAttachement(bubble, animator: self.animator!)
        }
        self.addColision(views, animator: animator)
        self.addDynamicBevaviour(views, animator: animator)
        
        views.forEach { (view) in
            let angle = Double(arc4random()) / (pow(2.0, 32.0) - 1) * M_PI * 2.0;
            self.applyPush(view, angle: CGFloat(angle), magnitude: 3)
        }
    }
    
    func numberOfBubbles() -> Int {
        var number = 0
        if let nr = self.dataSource?.numberOfBubbles() {
            number = nr
        }
        return number
    }
    
    func bubbleTitle(atIndex:Int) -> String {
        var title = ""
        if let t = self.dataSource?.bubbleTitle(atIndex) {
            title = t
        }
        return title
    }
    
    func bubbleSubtitle(atIndex:Int) -> String {
        var subtitle = ""
        if let st = self.dataSource?.bubbleSubtitle(atIndex) {
            subtitle = st
        }
        return subtitle
    }
    
    func configureBubble(bubble: VTBubbleView, atIndex: Int) {
        if let color = self.delegate?.bubbleColor?() {
            bubble.backgroundColor = color
        } else {
            bubble.backgroundColor = self.bubbleColor
        }
        
        if let hightlightedColor = self.delegate?.highlightedBubbleColor?() {
            bubble.highlightedColor = hightlightedColor
        } else {
            bubble.highlightedColor = self.highlightedBubbleColor
        }
        var size = self.bubbleSize

        if let highlighted = self.dataSource?.isBubbleHighlighted(atIndex) {
            if highlighted {
                size = self.highlightedBubbleSize
                bubble.state = .Highlighted
            } else {
                bubble.state = .Normal
            }
        }
        
        bubble.clipsToBounds = true
        bubble.frame = CGRect(origin: self.bubblePosition(atIndex, itemSize: size), size: size)
    }

    func bubblesRequiredBoundarySize(numberOfViews: Int, itemSize:CGSize) -> CGSize {
        var requiredSize = self.bubblesRequiredViewSize(numberOfViews, itemSize: itemSize)
        requiredSize.width = requiredSize.width * 2
        return requiredSize
    }
    
    func bubblesRequiredViewSize(numberOfViews: Int, itemSize:CGSize) -> CGSize {
        let height = CGRectGetHeight(self.frame)
        let numberOfRows = Int(height / itemSize.height)
        let numberOfColums = Int(ceil(Double(numberOfViews / numberOfRows)))
        let width = CGFloat(CGFloat(numberOfColums) * CGFloat(itemSize.width))
        return CGSize(width: width, height: height)
    }
    
    func bubblePosition(atIndex: Int, itemSize: CGSize) -> CGPoint {
        let height = CGRectGetHeight(self.frame)
        let numberOfRows = Int(height / itemSize.height)        
        let column = Int(atIndex / numberOfRows)
        let row = Int(fmod(Float(atIndex), Float(numberOfRows)))
        return CGPoint(x: CGFloat(column) * itemSize.width, y: CGFloat(row) * itemSize.height)
    }
    
    func addAttachement(bubble: UIDynamicItem, animator: UIDynamicAnimator) {
        let attachment = UIAttachmentBehavior(item: bubble, attachedToAnchor: CGPoint(x: CGRectGetMidX(self.bounds), y: CGRectGetMidY(self.bounds)))
        attachment.length = 50
        attachment.damping = 0.3
        attachment.frequency = 0.09
        animator.addBehavior(attachment)
    }
    
    func addColision(bubbles: [UIDynamicItem], animator: UIDynamicAnimator) {
        let collision = UICollisionBehavior(items: bubbles)
        let margin = (self.bubblesRequiredBoundarySize(bubbles.count, itemSize: (bubbles.first?.bounds.size)!).width - CGRectGetWidth(self.frame)) / 2
        collision.setTranslatesReferenceBoundsIntoBoundaryWithInsets(UIEdgeInsets(top: 0, left: -margin, bottom: 0, right: -margin))
        collision.collisionMode = .Everything
        animator.addBehavior(collision)
    }

    func addDynamicBevaviour(bubbles: [UIDynamicItem], animator: UIDynamicAnimator) {
        let behaviour = UIDynamicItemBehavior(items: bubbles)
        behaviour.elasticity = 0.6
        behaviour.friction = 0.1
        behaviour.charge = 1
        behaviour.resistance = 0.6
        behaviour.angularResistance = 0.6
        behaviour.allowsRotation = false
        animator.addBehavior(behaviour)
    }
    
    func handleTap(gesture: UITapGestureRecognizer) {
        
        var size: CGSize
        var state: VTBubbleState
        
        guard let gestureView = gesture.view as? VTBubbleView else {
            return
        }
        
        if (gestureView.backgroundColor == UIColor.textColor()) {
            let siblings = self.animator?.itemsInRect(CGRectInset(gestureView.frame, -200, -200))
            
            for view in siblings! {
                if view.center != gestureView.center {
                    self.applyPush(view, angle: self.angle(view.center, point2: gestureView.center), magnitude: 0.6)
                }
            }
            state = .Highlighted
            size = self.highlightedBubbleSize
        } else {
            state = .Normal
            size = self.bubbleSize
        }
        
        gestureView.state = state
        
        self.resize(gestureView, frame: CGRect(origin: gestureView.frame.origin, size: size)) {
            self.tickleBehaviours(gestureView)
            self.animator?.updateItemUsingCurrentState(gestureView)
        }
    }
    
    func resize(view: UIView, frame: CGRect, completion: (Void) -> Void) {
        let duration = 0.2
        let radius = CGRectGetHeight(frame) * 0.5
        let initialRadius = view.layer.cornerRadius
        
        UIView.animateKeyframesWithDuration(duration, delay: 0, options: .LayoutSubviews, animations: {
            view.bounds = CGRect(origin: CGPointZero, size: frame.size)
        }) { (finished) in
            if finished {
                completion()
            }
        }
        
        let animation = CABasicAnimation(keyPath: "cornerRadius")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.fromValue = initialRadius
        animation.toValue = radius
        animation.duration = duration
        view.layer.cornerRadius = radius
        view.layer.addAnimation(animation, forKey: "cornerRadius")
    }
    
    func tickleBehaviours(view: UIView) {
        var viewsBehaviours = Array<UIDynamicBehavior>()
        
        for behaviour in self.animator!.behaviors {
            
            if let col = behaviour as? UICollisionBehavior {
                for item  in col.items {
                    if view.center == item.center {
                        viewsBehaviours.append(col)
                        self.animator?.removeBehavior(col)
                    }
                }
            }
            
            if let attach = behaviour as? UIAttachmentBehavior {
                for item  in attach.items {
                    if view.center == item.center {
                        viewsBehaviours.append(attach)
                        self.animator?.removeBehavior(attach)
                    }
                }
                
            }
            
            if let beh = behaviour as? UIDynamicItemBehavior {
                for item  in beh.items {
                    if view.center == item.center {
                        viewsBehaviours.append(beh)
                        self.animator?.removeBehavior(beh)
                    }
                }
                
            }
            
            if let push = behaviour as? UIPushBehavior {
                for item in push.items {
                    if view.center == item.center {
                        viewsBehaviours.append(push)
                        self.animator?.removeBehavior(push)
                    }
                }
                
            }
        }
        
        for behaviour in viewsBehaviours {
            self.animator?.addBehavior(behaviour)
        }
    }
    
    func handlePan(gesture: UIPanGestureRecognizer) {
        if gesture.state == UIGestureRecognizerState.Ended {
            let p = gesture.locationInView(self)
            let o = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))
            let angle = self.angle(p, point2: o)
            
            let siblings = self.animator?.itemsInRect(CGRectInset(gesture.view!.frame, -200, -200))
            
            for view in siblings! {
                if view.center != gesture.view!.center {
                    self.applyPush(view, angle: angle, magnitude: 1)
                }
            }
        }
    }
    
    func angle(point: CGPoint, point2: CGPoint) -> CGFloat {
        return atan2(point.y-point2.y, point.x-point2.x)
    }
    
    func applyPush(view: UIDynamicItem, angle: CGFloat, magnitude: CGFloat) {
        
        let push = UIPushBehavior(items: [view], mode: UIPushBehaviorMode.Instantaneous)
        animator?.addBehavior(push)
        push.magnitude = magnitude
        push.angle = angle
    }
}
