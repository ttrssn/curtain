//
//  VTIntroViewCell.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 04/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import Cartography

class VTIntroViewCell: UIView {
    var image: UIImage?
    var title: String?
    var details: String?
    
    @IBOutlet weak private var imageView: UIImageView?
    @IBOutlet weak private var titleView: VTHeadlineLabel?
    @IBOutlet weak private var detailsView: VTTextView?
    
    func setup(image: UIImage?, title: String, details: String) {
        backgroundColor = UIColor.backgroundColor()
        imageView?.image = image
        titleView?.text = title
        titleView?.numberOfLines = 1
        titleView?.textColor = UIColor.textColor()
        detailsView?.text = details
        detailsView?.textAlignment = .Center
        detailsView?.textColor = UIColor.textColor()
    }
    
}
