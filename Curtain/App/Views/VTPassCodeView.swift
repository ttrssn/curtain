//
//  VTPassCodeView.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 05/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import AudioToolbox

enum VTLockState {
    case EnterPasscode
    case SetupPasscode
    case ConfirmPasscode
    case ChangePasscode
    case RemovePasscode
    case InvalidPasscode
    
    func nextState() -> VTLockState {
        switch self {
        case .EnterPasscode:
            return .SetupPasscode
        case .SetupPasscode:
            return .ConfirmPasscode
        case .ConfirmPasscode:
            return .ChangePasscode
        case .ChangePasscode:
            return .RemovePasscode
        case .RemovePasscode:
            return .InvalidPasscode
        case .InvalidPasscode:
            return .EnterPasscode
        }
    }
}

class VTPassCodeView: UIView {

    @IBOutlet weak var title: VTHeadlineLabel?
    @IBOutlet weak var details: VTLabel?
    @IBOutlet var dots: [VTDotView]?
    @IBOutlet var numbers: [VTPasscodeButton]?

    var viewModel = VTPassCodeViewModel.viewModel(VTLockState.EnterPasscode)
    
    var state = VTLockState.EnterPasscode {
        didSet {
            updateUI(VTPassCodeViewModel.viewModel(self.state))
        }
    }
    
    @IBAction func didTapNumber(sender: AnyObject) {
        playClick()
        
        guard let dots = self.dots else {
            return
        }
        
        for dot in dots {
            if dot.isHighlighted == false {
                dot.isHighlighted = true
                break
            }
        }
        
        if dots.filter({$0.isHighlighted == false}).count == 0 {
            proceedToNextState(state)
        }
    }
    
    @IBAction func didTapDelete(sender: AnyObject) {
        playClick()
        if let dot = dots?.filter({$0.isHighlighted == true}).last {
            dot.isHighlighted = false
        }
        
    }
    
    func proceedToNextState(currentState: VTLockState) {
        self.state = self.state.nextState()
        
        guard let dots = self.dots else {
            return
        }
        
        dots.forEach { dot in
            dot.isHighlighted = false
        }
    }
    
    func updateUI(viewModel: VTPassCodeViewModel) {
        self.title?.setText(viewModel.title, animated: true)
        self.details?.setText(viewModel.details, animated: true)
    }

    func playClick() {
        AudioServicesPlaySystemSound(1105);
    }
}
