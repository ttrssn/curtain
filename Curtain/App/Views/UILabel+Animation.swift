//
//  UILabel+Animation.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 06/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
let animationDuration = 0.4

extension UILabel {
    
    func setText(text: String, animated: Bool) {
        if animated {
            let animation = CATransition()
            animation.duration = animationDuration;
            animation.type = kCATransitionFade;
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            self.layer.addAnimation(animation, forKey: "changeTextTransition")
            self.text = text;
        } else {
            self.layer.removeAllAnimations()
            self.text = text;
        }
    }
    
    func animate(titles titles: Array<String>, duration: Double) {
        
        let gap = duration / Double(titles.count)
        var delay = 0.0
        
        for string in titles {
            self .performSelector(#selector(UILabel.changeText(_:)), withObject: string, afterDelay: delay)
            delay += gap
        }
    }
    
    func changeText(text: String) {
        self.setText(text, animated: true)
    }
}