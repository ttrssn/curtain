//
//  VTHeadlineLabel.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 04/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

class VTHeadlineLabel: VTLabel {
    
    override func defaultFont() -> UIFont {
        let descriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(UIFontTextStyleTitle1)
        
        if let size = descriptor.objectForKey(UIFontDescriptorSizeAttribute) as? NSNumber {
            let cgfloatvalue = size.floatValue
            return UIFont.headlineFontWithSize(CGFloat(cgfloatvalue))
        }
        return UIFont.headlineFontWithSize(UIFont.labelFontSize())
    }
}
