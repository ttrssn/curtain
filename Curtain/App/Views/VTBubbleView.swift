//
//  VTBubbleView.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 13/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import Cartography

enum VTBubbleState {
    case Highlighted
    case Normal
    
    static func textColor(state: VTBubbleState) -> UIColor {
        switch state {
        case .Highlighted:
            return UIColor.whiteColor()
        case .Normal:
            return UIColor.whiteColor()
        }
    }
    
    static func backgroundColor(state: VTBubbleState) -> UIColor {
        switch state {
        case .Highlighted:
            return UIColor.currentPageIndicatorColor()
        case .Normal:
            return UIColor.textColor()
        }
    }
    
    static func textSize(state: VTBubbleState) -> CGFloat {
        switch state {
        case .Highlighted:
            return 18
        case .Normal:
            return 12
        }
    }
}

class VTBubbleView: UIView {

    var title: String?
    var subtitle: String?
    var highlightedColor = UIColor.whiteColor()
    var state: VTBubbleState = .Normal {
        didSet {
            backgroundColor = VTBubbleState.backgroundColor(self.state)
            titleLabel?.textColor = VTBubbleState.textColor(self.state)
            titleLabel?.font = UIFont.bodyFontWithSize(VTBubbleState.textSize(self.state))
        }
    }
    private var titleLabel: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(title: String) {
        self.init(frame: CGRectZero)
        self.title = title
        self.setup()
    }
    
    convenience init(title: String, subtitle: String) {
        self.init(frame: CGRectZero)
        self.title = title
        self.subtitle = subtitle
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override var collisionBoundsType: UIDynamicItemCollisionBoundsType {
        return .Ellipse
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = CGRectGetHeight(self.bounds) * 0.5
    }
    
    func setup() {
        self.subviews.forEach({view in
            view.removeFromSuperview()
        })
        
        let titleLabel = UILabel()
        titleLabel.textColor = VTBubbleState.textColor(self.state)
        titleLabel.font = UIFont.bodyFontWithSize(VTBubbleState.textSize(self.state))
        titleLabel.text = self.title
        titleLabel.minimumScaleFactor = 0.5
        titleLabel.textAlignment = .Center
        titleLabel.allowsDefaultTighteningForTruncation = true
        titleLabel.adjustsFontSizeToFitWidth = true
        self.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        constrain(titleLabel, self) { label, superview in
            label.left == superview.left + 5
            label.right == superview.right - 5
            label.centerY == superview.centerY
        }
        self.backgroundColor = VTBubbleState.backgroundColor(self.state)
    }

}
