//
//  VTLabel.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 04/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

class VTLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupNotifications()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupNotifications()
    }
    
    func setupNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(VTLabel.updateAppearance), name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    func updateAppearance() {
        self.font = defaultFont()
        invalidateIntrinsicContentSize()
    }
    
    deinit {
       NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func willMoveToWindow(newWindow: UIWindow?) {
        super.willMoveToWindow(window)
        updateAppearance()
    }
    
    func defaultFont() -> UIFont {
        let descriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(UIFontTextStyleBody)
        
        if let size = descriptor.objectForKey(UIFontDescriptorSizeAttribute) as? NSNumber {
            let cgfloatvalue = size.floatValue
            return UIFont.bodyFontWithSize(CGFloat(cgfloatvalue))
        }
        return UIFont.bodyFontWithSize(UIFont.labelFontSize())
    }
    
}
