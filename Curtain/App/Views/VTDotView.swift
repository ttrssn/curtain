//
//  VTDotView.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 05/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

@IBDesignable

class VTDotView: UIView {

    @IBInspectable
    var highlightedColor: UIColor = UIColor.currentPageIndicatorColor() {
        didSet {
            setupView()
        }
    }
    
    @IBInspectable
    var isHighlighted: Bool = false {
        didSet {
            setupView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSizeMake(16, 16)
    }
    
    func setupView() {
        let normalColor = UIColor.textColor()
        layer.cornerRadius = self.frame.size.height * 0.5
        self.backgroundColor = isHighlighted ? highlightedColor : normalColor
    }
}
