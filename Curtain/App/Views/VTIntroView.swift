//
//  VTIntroView.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 01/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit
import Cartography

protocol VTIntroViewDelegate : class {
    func didTapSkip(sender: VTIntroView)
}

protocol VTIntroViewDatasource : class {
    func numberOfPages(sender: VTIntroView) -> Int
    func pageAtIndex(index: Int, sender: VTIntroView) -> VTIntroViewCell
}

class VTIntroView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var collectionView: UICollectionView?
    var pageControl: UIPageControl?
    var skipButton: UIButton?
    
    weak var flowLayout: UICollectionViewFlowLayout?
    weak var delegate: VTIntroViewDelegate?
    weak var dataSource: VTIntroViewDatasource?
    
    //MARK: Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init() {
        self.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setup()
    }
    
    //MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfPages()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cellReuseIdentifier", forIndexPath: indexPath)
        configureCell(cell, atIndex:indexPath)
        return cell
    }
 
    //MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return self.bounds.size
    }
    
    //MARK: UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let width = scrollView.frame.size.width
        let contentOffset = scrollView.contentOffset.x
        let page = floor((contentOffset - width / 2) / width) + 1;
        pageControl?.currentPage = Int(page)
    }
    
    // MARK: Observe
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "currentPage" {
            if let changes = change {
                let value = changes["new"]
                if value?.intValue > 0 {
                    self.skipButton?.hidden = true
                } else {
                    self.skipButton?.hidden = false
                }
            }
        }
    }
    
    //MARK: Private
    
    func setup() {
        
        // reset
        
        for view in self.subviews {
            view.removeFromSuperview()
        }
        
        backgroundColor = UIColor.backgroundColor()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .Horizontal
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        self.flowLayout = flowLayout
        
        let collectionView = UICollectionView(frame: self.bounds, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = UIColor.clearColor()
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.pagingEnabled = true
        collectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cellReuseIdentifier")
        self.collectionView = collectionView
        addSubview(collectionView)
        
        constrain(self, collectionView) { superview, cv in
            cv.top == superview.top
            cv.bottom == superview.bottom
            cv.left == superview.left
            cv.right == superview.right
        }
        
        let button = UIButton(type: UIButtonType.Custom)
        button.setTitle("Skip", forState: .Normal)
        button.setTitle("Skip", forState: .Highlighted)
        button.setTitleColor(UIColor.textColor(), forState: .Normal)
        button.setTitleColor(UIColor.textColor(), forState: .Highlighted)
        button.titleLabel?.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
        button.addTarget(self, action: #selector(VTIntroView.tappedSkipButton), forControlEvents: UIControlEvents.TouchUpInside)
        self.skipButton = button
        addSubview(button)
        
        constrain(button, self) { button, superview in
            button.right == superview.right - 20
            button.top == superview.top + 20
        }
        
        let pageControl = UIPageControl()
        pageControl.defersCurrentPageDisplay = true
        pageControl.numberOfPages = numberOfPages()
        pageControl.currentPageIndicatorTintColor = UIColor.currentPageIndicatorColor()
        pageControl.pageIndicatorTintColor = UIColor.pageIndicatorColor()
        self.pageControl = pageControl
        addSubview(pageControl)
        
        constrain(pageControl, self) { pageControl, superview in
            pageControl.centerX == superview.centerX
            pageControl.height == 20
            pageControl.left == superview.left
            pageControl.right == superview.right
            pageControl.bottom == superview.bottom - 20
        }
        
        pageControl.addObserver(self, forKeyPath: "currentPage", options: .New, context: nil)
        
    }
    
    func configureCell(cell: UICollectionViewCell, atIndex: NSIndexPath) {
        cell.backgroundView = nil
        cell.backgroundColor = UIColor.clearColor()
        
        for view in cell.subviews {
            view.removeFromSuperview()
        }
        
        let view = pageAtIndex(atIndex.row)
        cell.addSubview(view)
        
        constrain(view, cell) { view, cell in
            view.edges == cell.edges
        }
    }
    
    func numberOfPages() -> Int {
        guard let datasource = self.dataSource else {
            return 0
        }
        return datasource.numberOfPages(self)
    }
    
    func pageAtIndex(index: Int) -> VTIntroViewCell {
        guard let datasource = self.dataSource else {
            return VTIntroViewCell()
        }
        return datasource.pageAtIndex(index, sender: self)
    }
    
    func tappedSkipButton() {
        if let delegate = self.delegate {
            delegate.didTapSkip(self)
        }
    }
    
}
