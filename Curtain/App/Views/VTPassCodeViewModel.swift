//
//  VTPassCodeViewModel.swift
//  Curtain
//
//  Created by Victor Tatarasanu on 06/04/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import UIKit

class VTPassCodeViewModel: NSObject {
    var title : String = ""
    var details: String = ""
    
    class func viewModel(state: VTLockState) -> VTPassCodeViewModel {
        let viewModel = VTPassCodeViewModel()
        
        switch state {
            
            case .EnterPasscode:
                viewModel.title = "Enter Passcode"
                viewModel.details = "Enter your passcode to continue"
            break
            case .SetupPasscode:
                viewModel.title = "Enter a Passcode"
                viewModel.details = "Choose your passcode"
            break
            case .ConfirmPasscode:
                viewModel.title = "Confirm Passcode"
                viewModel.details = "Enter the passcode again"
            break
            case .ChangePasscode:
                viewModel.title = "Enter Old Passcode"
                viewModel.details = "Enter your old passcode again"
            break
            case .RemovePasscode:
                viewModel.title = "Enter Passcode"
                viewModel.details = "Enter your passcode to continue"
            break
            case .InvalidPasscode:
                viewModel.title = "Try again!"
                viewModel.details = "Passcodes didn\'t match."
            break
        }
        
        return viewModel
    }
    
}
