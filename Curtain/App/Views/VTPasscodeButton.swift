//
//  PasscodeSignButton.swift
//  PasscodeLock
//
//  Created by Yanko Dimitrov on 8/28/15.
//  Copyright © 2015 Yanko Dimitrov. All rights reserved.
//

import UIKit

@IBDesignable
public class VTPasscodeButton: UIButton, UIInputViewAudioFeedback {
    
    @IBInspectable
    public var passcodeSign: String = "1"
    
    public var borderRadius: CGFloat {
        get {
            return self.frame.size.height * 0.5
        }
        set {
            self.borderRadius = newValue
            self.setupView()
        }
    }

    public var normalColor: UIColor = UIColor.textColor() {
        didSet {
            setupView()
        }
    }

    public var highlightedColor: UIColor = UIColor.currentPageIndicatorColor() {
        didSet {
            setupView()
        }
    }
    
    public override func intrinsicContentSize() -> CGSize {
        return CGSizeMake(75, 75)
    }
    
    public override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupView()
    }

    private func setupView() {
        layer.borderWidth = 1
        layer.cornerRadius = borderRadius
        layer.borderColor = normalColor.CGColor
        self.setTitleColor(normalColor, forState: .Normal)
        self.setTitleColor(highlightedColor, forState: .Highlighted)
    }
    
    public override var highlighted: Bool {
        didSet {
            animateBackgroundColor(self.highlighted ? self.highlightedColor : self.normalColor)
        }
    }

    private func animateBackgroundColor(color: UIColor) {
        
        UIView.animateWithDuration(
            1,
            delay: 0.0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 0.0,
            options: [.AllowUserInteraction],
            animations: {
                self.layer.borderColor = color.CGColor
            },
            completion: nil
        )
    }
}
