//
//  FilterControlProvider.swift
//  FilterControlProvider
//
//  Created by Victor Tatarasanu on 22/03/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import NetworkExtension

class FilterControlProvider: NEFilterControlProvider {

	override func startFilterWithCompletionHandler(completionHandler: (NSError?) -> Void) {
		// Add code to initialize the filter
		completionHandler(nil)
	}

	override func stopFilterWithReason(reason: NEProviderStopReason, completionHandler: () -> Void) {
		// Add code to clean up filter resources
		completionHandler()
	}

	override func handleNewFlow(flow: NEFilterFlow, completionHandler: (NEFilterControlVerdict) -> Void) {
		// Add code to determine if the flow should be dropped or not, downloading new rules if required
		completionHandler(.allowVerdictWithUpdateRules(false))
	}
}
