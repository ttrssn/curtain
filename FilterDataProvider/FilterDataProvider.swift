//
//  FilterDataProvider.swift
//  FilterDataProvider
//
//  Created by Victor Tatarasanu on 22/03/16.
//  Copyright © 2016 Victor Tatarasanu. All rights reserved.
//

import NetworkExtension

class FilterDataProvider: NEFilterDataProvider {

	override func startFilterWithCompletionHandler(completionHandler: (NSError?) -> Void) {
		// Add code to initialize the filter.
		completionHandler(nil)
	}

	override func stopFilterWithReason(reason: NEProviderStopReason, completionHandler: () -> Void) {
		// Add code to clean up filter resources.
		completionHandler()
	}

	override func handleNewFlow(flow: NEFilterFlow) -> NEFilterNewFlowVerdict {
		// Add code to determine if the flow should be dropped or not, downloading new rules if required.
		return .allowVerdict()
	}
}
